package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    //Creamos las instancia
    TextView Name, Price, Descriptio;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        //Les damo su respectivo ID a las instancias creadas
        Name = (TextView)findViewById(R.id.TXTNombre);
        Price = (TextView)findViewById(R.id.TXTPrecio);
        Descriptio = (TextView)findViewById(R.id.TXTDescription);
        imageView = (ImageView)findViewById(R.id.imagen);

        // INICIO - CODE6
        // 3.1 Se objeto de tipo String, la cual recibe el parametro enviado de ResultsActivity a travez de PuxExtras
        String object_id = getIntent().getExtras().getString("object_id");

        DataQuery dataQuery = DataQuery.get("item");
        //Al utilizar el metodo getInBackground este nos pedira un String, en el cual debemos poner el creado anteriormente
        dataQuery.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {

                if (e==null) {

                    //3.3 y 3.4
                    //Accedemos y hacemos uso de las propiedades que contiene object
                    //con SetText rrellenamos los campos de texto y con setImagenBitmap el ImagenView
                    Name.setText((String) object.get("name"));
                    Price.setText((String) object.get("price") + " " + getResources().getString(R.string.signoDolar));
                    Descriptio.setText((String) object.get("description"));
                    imageView.setImageBitmap((Bitmap) object.get("image"));
                    //sirve para activar el scrollbars
                    Descriptio.setMovementMethod(LinkMovementMethod.getInstance());

                }else {
                    //error
                }


            }
        });


        // FIN - CODE6

    }

}
